import React from 'react';
import { Platform,View, Text, TouchableOpacity, StyleSheet, Image, SafeAreaView, ScrollView, TextInput} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';



export default function App() {
  return (
    
    <LinearGradient colors={['#F27281', '#345D7E']} style={styles.container}>
      <View style={styles.headerNotificacao}></View>
    <View>
        <View style={styles.logoBack}>
          <View style={styles.logoContainer}>
            <TouchableOpacity>
              <Image style={styles.buttonVoltar} source={require('./assets/voltar.png')} /> 
            </TouchableOpacity>
            <Image style={styles.logo} source={require('./assets/logosemfundo.png')} />
          </View>
        </View>

        <View>
            <View style={styles.containerContent}  >
              <View style={flexDirection = 'column'}>
                <Text style={styles.colaborador}>Olá: Maria do Carmo</Text>
                <Text style={styles.local}>Você está no local:  Copa</Text>
                <Text style={styles.cliente}>Do cliente: SPSP</Text>


                <SafeAreaView style={styles.safeAreaViewInstrucao}> 
                    
                    <View>
                        <Text  style={styles.tituloRow}>INFORME QUAL MANUTENÇÃO FOI REALIZADA NO LOCAL</Text>
                    </View>


                    <View style={styles.MainContainer}>
                      <TextInput
                          style={styles.TextInputStyleClass}
                          underlineColorAndroid="transparent"
                          placeholder={"Observação"}
                          placeholderTextColor={"#9E9E9E"}
                          multiline={true}
                        />
                    </View>

                    <View style={styles.contentCamera}>

                      <TouchableOpacity  style={styles.buttonCamera}>
                        <Image style={styles.camera} source={require('./assets/camera2.png')} /> 
                      </TouchableOpacity>

                      

                        <View  style={styles.buttonFoto}>
                          <Text style={styles.buttonTextFoto}>Nenhuma Foto</Text>
                        </View>

                      </View>
                        
                      <TouchableOpacity  style={styles.button}>
                        <Text style={styles.buttonText}>Enviar</Text>
                      </TouchableOpacity>
                     
                 </SafeAreaView>
              </View>
            </View>
        </View>
    </View>
</LinearGradient>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 25,
    flex: 1,
    flexDirection: 'column',
  },
  containerContent: {
      flex:  1,
      justifyContent: 'center',
      alignItems: 'center'
  }, 
  safeAreaViewInstrucao: {
    alignContent: 'center',
    backgroundColor: '#C4C4C4',
    height: 370,
    width: 290,
    paddingRight: 5,
    marginVertical: 5,
    paddingLeft: 5,
    alignSelf: 'center',

  },
  
  logoContainer: {
    flexDirection: 'row',
  },
  logo: {
    marginLeft: "20%",
    width: 40,
    height: 20,
    paddingHorizontal: 40,
    paddingVertical: 40,
    resizeMode: 'contain',
    marginTop: -18,   
  },
  logoBack: {
    width: '100%',
    height: 40,
    backgroundColor: '#fff',
  },
  // local Onde A funcionaria esta no momento
  local: {
    fontSize: 18,
    marginHorizontal: 5,
  },
  // colaborador (funcionario da empresa que presta serviço)
  colaborador: {
    fontSize: 18,
    marginHorizontal: 5,
    marginTop: 10,
  },
  // cliente (empresa que contrata o serviço)
  cliente: {
    fontSize: 18,
    marginHorizontal: 5,
  },

  //botao de voutar
  buttonVoltar: {
    marginTop: 5,
    marginLeft: 10,   
    width: 40,
    height: 35,
    resizeMode: 'contain',
  },
  
  // campo do titulo do row 
  tituloRow: {
    fontSize: 15,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  rotinaText: {
    alignSelf: 'center'
  },
  headerNotificacao: {
    width: '100%',
    height: 25,
    marginTop: -27,
    backgroundColor: '#000'
  },
  MainContainer: {
    flex:1,
    paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
    marginTop:20,
    marginBottom: 110,
    marginHorizontal: 15
    },
   
    TextInputStyleClass:{
      fontSize: 20,
      textAlign: 'center',
      height: 200,
      borderColor: '#696969',
      borderRadius: 2,
      backgroundColor : "#FFFFFF",
      //height: 200
      },
      button: {
        alignSelf: 'center',
          height: 30,
          width: 250,
          backgroundColor: '#57BC90',
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 2,
          marginBottom: 5
      },
      buttonText: {
          color: '#FFF',
          fontWeight: 'bold',
          fontSize: 14,
      },
      buttonCamera: {
        height: 50,
        width: 60,
        //backgroundColor: '#57BC90',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        
      },
      camera: {
        width: 40,
        height: 40,
      },

      buttonFoto: {
          alignSelf: 'center',
          height: 30,
          width: 180,
          backgroundColor: '#FFF',
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 2,
          
      },
      buttonTextFoto: {
        color: '#9E9E9E',
        fontWeight: 'bold',
        fontSize: 14,
      },
      contentCamera: {
      
        flexDirection: 'row',
      }

});
