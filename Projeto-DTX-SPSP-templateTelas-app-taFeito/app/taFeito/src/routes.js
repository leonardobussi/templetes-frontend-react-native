import { createAppContainer, createSwitchNavigator } from 'react-navigation';

//importação de todas as páginas

import pageTemplate from './pages/pageTemplate';


const Routes = createAppContainer(
    createSwitchNavigator({
        pageTemplate,
        
        
    })
);

export default Routes;