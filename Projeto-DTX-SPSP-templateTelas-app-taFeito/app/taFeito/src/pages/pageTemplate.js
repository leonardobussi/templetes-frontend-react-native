import React from 'react';

import { View, Image, StyleSheet, Text } from 'react-native';

import { LinearGradient } from 'expo-linear-gradient';





export default pageTemplate = () => {



    return (
        <LinearGradient colors={['#F27281', '#345D7E']} style={styles.container}>
            <View>
                <View style={styles.logoBack}></View>
                <View style={styles.logoContainer}>
                    <Image style={styles.buttonVoltar} source={require('../../src/assets/voltar.png')} />
                    <Image style={styles.logo} source={require('../../src/assets/logo2.png')} />
                </View>
                <View>
                    <Text>Hello</Text>
                </View>
            </View>
        </LinearGradient>
        
    )
};


const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      maxWidth: 540,
      maxHeight: 960,
    },
    logoBack: {
        position: 'absolute',
        width: 400,
        height: 60,
        backgroundColor: '#fff',
    },
    logoContainer: {
        flexDirection: 'row',
    },
    logo: {
        marginVertical: 0,
        marginLeft: 82,
        width: 200,
        height: 60,
        paddingHorizontal: 0,
        paddingVertical: 0,
        resizeMode: 'contain',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    buttonVoltar: {
        alignContent: 'center',
        marginVertical: 10,
        marginLeft: 10,
        marginRight: 30,
        width: 40,
        height: 35,
        paddingVertical: 0,
        paddingHorizontal: 0,
        resizeMode: 'contain',
    },
});
