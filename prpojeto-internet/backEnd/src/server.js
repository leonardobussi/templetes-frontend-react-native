const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const path = require('path');

const routes = require('./routes');

const app = express();


mongoose.connect('mongodb://leo:leo@testeomnistack-shard-00-00-8rhl2.mongodb.net:27017,testeomnistack-shard-00-01-8rhl2.mongodb.net:27017,testeomnistack-shard-00-02-8rhl2.mongodb.net:27017/test?ssl=true&replicaSet=testeOmniStack-shard-0&authSource=admin&retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
app.use(cors());
app.use(express.json());
app.use('/files', express.static(path.resolve(__dirname, '..', 'uploads')));
app.use(routes);


app.listen(3333);