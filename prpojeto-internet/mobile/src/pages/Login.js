import React, {useState} from 'react';
import { View, Text, TextInput, Platform, TouchableOpacity, StyleSheet, KeyboardAvoidingView } from 'react-native';

import api from '../services/api';

export default function Login() {

    const [email, setEmail] = useState('');
    const [techs, setTechs] = useState('');


    async function handleSubmit(){
        const response = await api.post('/sessions', {
            email
        })

        const {_id} = response.data;
    }

    return( <KeyboardAvoidingView  enabled={Platform.OS === 'ios'} behavior="padding" style={styles.container}>
        <Text>AirCnC</Text>


        <View style={styles.form}>
            <Text style={styles.label}>SEU E-MAIL *</Text>
            <TextInput 
                style={styles.input}
                placeholder="Seu e-mail"
                placeholderTextColor="#999"
                keyboardType="email-address" 
                autoCapitalize="none"
                autoCorrect={false}
                value={email}
                onChangeText={setEmail}
            />


        <Text style={styles.label}>TECNOLOGIAS *</Text>
            <TextInput 
                style={styles.input}
                placeholder="Tecnologia de interesse"
                placeholderTextColor="#999" 
                autoCapitalize="words"
                autoCorrect={false}
                value={techs}
                onChangeText={setTechs}
            />

            <TouchableOpacity onPress={handleSubmit} style={styles.button}>
                <Text style={styles.buttonText}>Encontrar spots</Text>
            </TouchableOpacity>
        </View>
    </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex:  1,
        justifyContent: 'center',
        alignItems: 'center' 
    },

    label: {
        fontWeight: 'bold',
        color: '#444',
        marginBottom: 8,
    },

    form: {
        alignSelf: 'stretch',
        paddingHorizontal: 30,
        marginTop: 30,
    },

    input: {
        borderWidth: 1,
        borderColor: '#ddd',
        paddingHorizontal: 20,
        fontSize: 16,
        color: '#444',
        height: 44,
        marginBottom: 20,
        borderRadius: 2
    },

    button: {
        height: 42,
        backgroundColor: '#f05a5b',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
    },

    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,
    },
});