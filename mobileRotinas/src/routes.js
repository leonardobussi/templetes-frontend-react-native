import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import Rotinas from './pages/Rotinas';
import Confirmacao from './pages/Confirmacao';

const Routes = createAppContainer(
    createSwitchNavigator({
        Rotinas,
        Confirmacao,
    })
); 

export default Routes;