import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, SafeAreaView, SectionList, ScrollView} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';


export default function Confirmacao() {

    return (
        <LinearGradient colors={['#F27281', '#345D7E']} style={styles.container}>
            <View>
                <View style={styles.logoBack}>
                  <View style={styles.logoContainer}>
                    <TouchableOpacity>
                      <Image style={styles.buttonVoltar} source={require('../../src/assets/voltar.png')} /> 
                    </TouchableOpacity>
                    <Image style={styles.logo} source={require('../../src/assets/logosemfundo.png')} />
                  </View>
                </View>

                <View>
                    <View style={styles.containerContent}>
                      <View style={flexDirection = 'column'}>
                        <Text style={styles.colaborador}>Olá: Maria do Carmo</Text>
                        <Text style={styles.local}>Você está no local:  Copa</Text>
                        <Text style={styles.cliente}>Do cliente: SPSP</Text>


                        <SafeAreaView style={styles.safeAreaViewInstrucao}> 

                              <View>
                                <Text  style={styles.tituloRow}>PRÓXIMAS ROTINAS PROGRAMADAS</Text>
                              </View>
                            
                            <View style={styles.contentRotina}>
                                
                                <Text style={styles.rotinaText}>PRODUÇÃO PROGRAMADA</Text>
                                <TouchableOpacity  style={styles.button}>
                                  <Text style={styles.buttonText}>REGISTRAR ROTINA ACIMA</Text>
                                </TouchableOpacity>
                            </View>
    
                         </SafeAreaView>
                      </View>
                    </View>
                </View>
            </View>
        </LinearGradient>
                 
        
        
        

    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
    },
    containerContent: {
        flex:  1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    button: {
      alignSelf: 'center',
        height: 30,
        width: 250,
        backgroundColor: '#57BC90',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,
    },
    safeAreaViewInstrucao: {
      alignContent: 'center',
      backgroundColor: '#C4C4C4',
      height: 380,
      width: 290,
      paddingRight: 5,
      marginVertical: 5,
      paddingLeft: 5,
      alignSelf: 'center',
  
    },
    InstruContainer: {
      alignSelf: 'center',
    },
    logoContainer: {
      flexDirection: 'row',
    },
    logo: {
      marginLeft: "20%",
      width: 40,
      height: 20,
      paddingHorizontal: 40,
      paddingVertical: 40,
      resizeMode: 'contain',   
    },
    logoBack: {
      width: '100%',
      height: 60,
      backgroundColor: '#fff',
    },
    // local Onde A funcionaria esta no momento
    local: {
      fontSize: 18,
      marginHorizontal: 5,
    },
    // colaborador (funcionario da empresa que presta serviço)
    colaborador: {
      fontSize: 18,
      marginHorizontal: 5,
      marginTop: 10,
    },
    // cliente (empresa que contrata o serviço)
    cliente: {
      fontSize: 18,
      marginHorizontal: 5,
    },

    //botao de voutar
    buttonVoltar: {
      marginTop: 20,
      marginLeft: 10,   
      width: 40,
      height: 35,
      resizeMode: 'contain',
    },
    // CONTEUDO ROTINAS
    contentRotina: {
      marginTop: 10
    },
    // campo do titulo do row 
    tituloRow: {
      fontSize: 15,
      textAlign: 'center',
      fontWeight: 'bold'
    },
    rotinaText: {
        alignSelf: 'center'
      }
});